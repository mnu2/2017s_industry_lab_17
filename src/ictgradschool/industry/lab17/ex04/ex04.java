package ictgradschool.industry.lab17.ex04;

import com.sun.javafx.image.BytePixelSetter;
import ictgradschool.Keyboard;
import sun.awt.windows.ThemeReader;

public class ex04 {
    private double width;
    private double length;
    private double radius = 100*Math.random();
    private int rectArea;
    private int cirlceArea;

    public void start(){
        System.out.println("Welcome to the Shape Area Calculator! \n");
        System.out.println("Enter the width of the rectangle: ");
        width = doubleConverter(Keyboard.readInput());
        System.out.println("Enter the length of the rectangle: ");
        length = doubleConverter(Keyboard.readInput());
        System.out.println("The radius of the circle is: "  + Math.round(radius) + "\n");

        rectArea = intConverter(areaCalc(width, length));
        cirlceArea = intConverter(areaCircle());

        System.out.println("The smaller area is: " + compare(rectArea, cirlceArea));


    }

    public double getRadius(){
        return radius;
    }

    public double doubleConverter(String s){

            return Math.abs(Double.parseDouble(s));

    }


    public static void main(String[] args) {
        ex04 ex04 = new ex04();
        ex04.start();
    }

    public double areaCalc(double width, double length){
        return  (width*length);
    }

    public double areaCircle(){
        return  Math.PI * Math.pow(radius, 2);
    }


    public int intConverter(double y){
        return (int)Math.round(y);
    }

    public int compare(int area1, int area2){
        if (area1 > area2){
            return area2;
            }
            else if (area1 < area2){
            return area1 ;
        }
        else {return 0;}
    }


}
