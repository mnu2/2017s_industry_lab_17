package ictgradschool.industry.lab17.ex01;

import com.sun.javafx.image.BytePixelSetter;
import org.junit.Before;
import org.junit.Test;

import javax.lang.model.element.VariableElement;
import javax.swing.*;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testTurn(){
        myRobot.turn();
        assertEquals(Robot.Direction.East, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void twoturnTest(){
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.South, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void threeturnTest(){
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.West, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void fourTurnTest(){
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test public void illegalMoveSouth(){
        myRobot.turn();
        myRobot.turn();
        Robot.RobotState state = myRobot.currentState();
        try {
            myRobot.move();
            fail();
        }
        catch (IllegalMoveException e){
            assertEquals(10, myRobot.currentState().row);
            assertEquals(1, myRobot.currentState().column);
            assertEquals(Robot.Direction.South, myRobot.currentState().direction);
        }
    }

    @Test public void illegalMoveWest(){
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        try {
            myRobot.move();
            fail();
        }
        catch (IllegalMoveException e){
            assertEquals(10, myRobot.currentState().row);
            assertEquals(1, myRobot.currentState().column);
            assertEquals(Robot.Direction.West, myRobot.currentState().direction);
        }
    }

    @Test
    public void testIllegalMoveEast() {
        boolean atRight = false;
        myRobot.turn();
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atRight = myRobot.currentState().column == 10;
            assertTrue(atRight);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().column);
        }
    }
    @Test
    public void validMoveNorth(){
        try{
            myRobot.move();
            assertEquals(9, myRobot.currentState().row);
            assertEquals(1, myRobot.currentState().column);
            assertEquals(Robot.Direction.North, myRobot.currentState().direction);
        }
        catch (IllegalMoveException e){
            fail();
        }
    }

    @Test
    public void validMoveEast(){
        try{
            myRobot.turn();
            myRobot.move();
            assertEquals(10, myRobot.currentState().row);
            assertEquals(2, myRobot.currentState().column);
            assertEquals(Robot.Direction.East, myRobot.currentState().direction);
        }
        catch (IllegalMoveException e){
            fail();
        }
    }

    @Test
    public void validMoveSouth(){
        try{
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();
            assertEquals(10, myRobot.currentState().row);
            assertEquals(1, myRobot.currentState().column);
            assertEquals(Robot.Direction.South, myRobot.currentState().direction);
        }
        catch (IllegalMoveException e){
            fail();
        }
    }

    @Test
    public void validMoveWest(){
        try{
            myRobot.turn();
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();
            assertEquals(10, myRobot.currentState().row);
            assertEquals(1, myRobot.currentState().column);
            assertEquals(Robot.Direction.West, myRobot.currentState().direction);
        }
        catch (IllegalMoveException e){
            fail();
        }
    }

    @Test
    public void testBackTrack(){
        try {
            myRobot.move();
            myRobot.turn();
            myRobot.move();
            myRobot.move();
            myRobot.turn();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
            fail();
        }
        Robot.RobotState state = myRobot.states.get(myRobot.states.size() - 2);
        myRobot.backTrack();
        assertEquals(myRobot.currentState(), myRobot.states.get(myRobot.states.size() - 1));
    }

    @Test
    public void testNullMoveBackTrack(){
            myRobot.backTrack();
            assertEquals(0, myRobot.states.size());
    }
}
