package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Implement this class.
 */
public class TestDictionary {

    Dictionary dictionary = new Dictionary();

    @Test
    public void testTrueIsTrue() {
        assertEquals(true, true);
    }

    @Test
    public void testCorrectSpelling(){
        for (String s: dictionary.WORDS.toLowerCase().split(",")){
                assertEquals(true, dictionary.isSpellingCorrect(s));
        }
    }
}

