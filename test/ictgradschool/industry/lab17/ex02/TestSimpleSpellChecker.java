package ictgradschool.industry.lab17.ex02;

import com.sun.xml.internal.bind.v2.model.core.ID;
import org.junit.Before;
import org.junit.Test;
import org.omg.IOP.ComponentIdHelper;

import java.util.ArrayList;
import java.util.FormatFlagsConversionMismatchException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Implement this class.
 */
public class TestSimpleSpellChecker {

    IDictionary dictionary = new Dictionary();

    @Test
    public void testFoundationsOfMathematics() {
        assertEquals(2, 1 + 1);
    }

    public void testNullEntry() {
        try {
            SimpleSpellChecker spellChecker = new SimpleSpellChecker(dictionary, null);
            fail();
        } catch (InvalidDataFormatException e) {
            assertEquals("Words to check should not be null", e.getMessage());
        }
    }

    @Test
    public void testgetMispelledWords() {

        try {
            SimpleSpellChecker simpleSpellChecker = new SimpleSpellChecker(dictionary, "toward");
            assertEquals(0, simpleSpellChecker.getMisspelledWords().size());
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testWordsWithNumbers() {

        try {
            SimpleSpellChecker simpleSpellChecker = new SimpleSpellChecker(dictionary, "2toward");
            assertEquals(0, simpleSpellChecker.getMisspelledWords().size());
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testCaseSensitivity() {
        try {
            SimpleSpellChecker spellChecker = new SimpleSpellChecker(dictionary, "TOWard");
            assertEquals(0, spellChecker.getMisspelledWords().size());
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMispelledWords() {
        try{
            SimpleSpellChecker simpleSpellChecker = new SimpleSpellChecker(dictionary, "djhuie Dialect toward");
            assertEquals(2, simpleSpellChecker.getMisspelledWords().size());
            SimpleSpellChecker simpleSpellChecker1 = new SimpleSpellChecker(dictionary, "");
            assertEquals(0, simpleSpellChecker1.getMisspelledWords().size());
        }
        catch (InvalidDataFormatException e){
            e.printStackTrace();
        }

    }

    @Test
    public void testFrequenceyMethod(){
        try{
            SimpleSpellChecker spellChecker = new SimpleSpellChecker(dictionary, "this this this this, alphabet, alphabet     ");
            assertEquals(4, spellChecker.getFrequencyOfWord("this"));
            assertEquals(2, spellChecker.getFrequencyOfWord("alphabet"));
            assertEquals(0, spellChecker.getFrequencyOfWord(" "));

            assertEquals(0, spellChecker.getFrequencyOfWord(null));
        }
        catch (InvalidDataFormatException e){}
    }

    @Test
    public void testUniqueWords(){
        try{
            SimpleSpellChecker spellChecker = new SimpleSpellChecker(dictionary, "a a a a b b b b trilobyte, trilobyte, warrant warrant warrant");
            assertEquals(4, spellChecker.getFrequencyOfWord("a"));
            assertEquals(4, spellChecker.getFrequencyOfWord("b"));
            assertEquals(2, spellChecker.getFrequencyOfWord("trilobyte"));
            assertEquals(3, spellChecker.getFrequencyOfWord("warrant"));
        }
        catch(InvalidDataFormatException e){
            e.printStackTrace();
        }
    }
}
