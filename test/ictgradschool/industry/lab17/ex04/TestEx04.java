package ictgradschool.industry.lab17.ex04;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

public class TestEx04 {

    private ex04 myex04;

    @Before
    public void setUP(){myex04 = new ex04();}

    //negative numbers are not tested as we take the absolute value of
    //the inputs.

    @Test
    public void testdoubleConverterInt() {

        assertEquals(3.0, myex04.doubleConverter("3"));
    }

    @Test
    public void testdoubleConverterDouble(){
        assertEquals(3.0, myex04.doubleConverter("3.00000000"));
    }

    @Test
    public void testdoubleConverterNeg(){
        assertEquals(3.0, myex04.doubleConverter("-3"));
    }

    @Test (expected = NullPointerException.class)
    public void testdoubleConverterNull(){
        assertFalse(myex04.doubleConverter(null) == 0);
    }

    @Test
    public void testAreaCalc(){
        assertEquals(36.0, myex04.areaCalc(4, 9));
        assertEquals(36.0, myex04.areaCalc(6, 6.000));
    }

    @Test
    public void testIntConverterRoundDown(){
        assertEquals(3, myex04.intConverter(3.35));

    }

    @Test
    public void testIntConverterRoundUp(){
        assertEquals(4, myex04.intConverter(3.55));
    }

    @Test
    public void testCircleArea(){
        assertEquals(Math.PI*Math.pow(myex04.getRadius(), 2), myex04.areaCircle());
    }

    @Test
    public void testAreaComparisonL(){
        assertEquals(25, myex04.compare(25, 35));
    }

    @Test
    public void testAreaComparisonR(){
        assertEquals(12, myex04.compare(100, 12));
    }

    @Test
    public void testAreaComparisonSame(){
        assertEquals(0, myex04.compare(101, 101));
    }


}
